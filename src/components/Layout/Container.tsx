import { Box, BoxProps } from '@pancakeswap/uikit'

const Container: React.FC<React.PropsWithChildren<BoxProps>> = ({ children, ...props }) => (
  <Box px={['16px', '10px']} paddingBottom="1.5rem" mx="auto" maxWidth="1150px"  {...props}>
    {children}
  </Box>
)

export default Container
