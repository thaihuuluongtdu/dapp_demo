import BigNumber from "bignumber.js"
import contracts from "config/constants/contracts"
import { getAddress } from "utils/addressHelpers"
import multiBuyBox from 'config/abi/multiBuyBox.json'
import multicall from "utils/multicall"
import { nftInMarket } from "./type"





export const fetchBalanceNftInMarkeMultiBuy = async (chainId:number): Promise<nftInMarket> => {
    const contractAddress = getAddress(contracts.runMarketplaceMultiByBox, chainId) || ""
    if ( contractAddress.length ) {
        try {
            const callsBalance = []
            const callsPrice = []
            for( let index=1; index<=4; index++){
                callsBalance.push({
                    address: getAddress(contracts.runMarketplaceMultiByBox, chainId),
                    name: 'runTogetherBoxNftTypeCountMap',
                    params: [index]
                })
                callsPrice.push({
                    address: getAddress(contracts.runMarketplaceMultiByBox, chainId),
                    name: 'runTogetherBoxNftTypePriceMap',
                    params: [index]
                })
            }
            const resultBalance = await multicall(multiBuyBox, callsBalance, chainId)
            const resultPrice = await multicall(multiBuyBox, callsPrice, chainId)
            
            return {
                nftInfo:{
                    nftBalance:{
                        totalNftMetaRush:Number(new BigNumber(resultBalance[0].toString())),
                        totalNftMetaRun:Number(new BigNumber(resultBalance[1].toString())),
                        totalNftMetaRace:Number(new BigNumber(resultBalance[2].toString())),
                        totalNftMetaRich:Number(new BigNumber(resultBalance[3].toString()))
                    },
                    nftPrice:{
                        totalNftMetaRush:Number(new BigNumber(resultPrice[0].toString()).dividedBy(1E18)),
                        totalNftMetaRun:Number(new BigNumber(resultPrice[1].toString()).dividedBy(1E18)),
                        totalNftMetaRace:Number(new BigNumber(resultPrice[2].toString()).dividedBy(1E18)),
                        totalNftMetaRich:Number(new BigNumber(resultPrice[3].toString()).dividedBy(1E18))
                    }
                }
            }
          } catch (e) {
            console.log(e)
            
            return {
                nftInfo:{
                    nftBalance:{
                        totalNftMetaRush:0,
                        totalNftMetaRun:0,
                        totalNftMetaRace:0,
                        totalNftMetaRich:0,
                    },
                    nftPrice:{
                        totalNftMetaRush:0,
                        totalNftMetaRun:0,
                        totalNftMetaRace:0,
                        totalNftMetaRich:0,
                    }
                }
            }
          }
    } else {
        return {
            nftInfo:{
                nftBalance:{
                    totalNftMetaRush:0,
                    totalNftMetaRun:0,
                    totalNftMetaRace:0,
                    totalNftMetaRich:0,
                },
                nftPrice:{
                    totalNftMetaRush:0,
                    totalNftMetaRun:0,
                    totalNftMetaRace:0,
                    totalNftMetaRich:0,
                }
            }
    }
 }
}