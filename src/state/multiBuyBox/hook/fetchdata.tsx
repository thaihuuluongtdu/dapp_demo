import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { AppDispatch, AppState } from "state"
import { fetchBalanceNftInMarkeMultiBuy } from ".."
import { fetchBalanceNftInMarket } from "../actions"



export const GetBalanceNftInMarket = (chainId:number) => {
    const marketMultiBuy = useSelector<AppState, AppState['marketMultiBuy']>((state) => state.marketMultiBuy)
    const nftInfo = marketMultiBuy.nftInfo
    const nftBalance = nftInfo.nftBalance
    const nftPrice = nftInfo.nftPrice
    const dispatch = useDispatch<AppDispatch>()
    useEffect(() => {
        const getNftBalanceInMarket = async () => {
            try {
                const result = await fetchBalanceNftInMarkeMultiBuy(chainId)
                dispatch(fetchBalanceNftInMarket(result))
               
            } catch (e) {
                console.log(e)
            }
        }
        getNftBalanceInMarket();
    }, [chainId, dispatch])
    return [ nftBalance, nftPrice ]
}