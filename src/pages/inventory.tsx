import { ChainId } from '@pancakeswap/sdk'
import Inventory from 'views/Inventory'

const InventoryPage = () => <Inventory />

InventoryPage.chains = [ChainId.BSC, ChainId.BSC_TESTNET]

export default InventoryPage