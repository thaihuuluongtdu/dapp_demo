import { ChainId } from '@pancakeswap/sdk'
import MyBalance from '../views/MyBalance'

const MyBalancePage = () => <MyBalance />

MyBalancePage.chains = [ChainId.BSC, ChainId.BSC_TESTNET, ChainId.ETHW_MAINNET]

export default MyBalancePage