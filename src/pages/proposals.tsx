import { ChainId } from '@pancakeswap/sdk'
import ComingSoon from 'views/ComingSoon'
import Proposals from '../views/Proposals'

const ProposalPage = () => <Proposals />

ProposalPage.chains =  [ChainId.BSC, ChainId.BSC_TESTNET]

export default ProposalPage