import { ChainId } from '@pancakeswap/sdk'
import MakeAProposal from '../views/MakeAProposals'

const MakeAProposalPage = () => <MakeAProposal />

MakeAProposalPage.chains = [ChainId.BSC, ChainId.BSC_TESTNET, ChainId.ETHW_MAINNET]

export default MakeAProposalPage