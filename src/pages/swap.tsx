import { CHAIN_IDS } from 'utils/wagmi'
import ComingSoon from 'views/ComingSoon'
import { ChainId } from '@pancakeswap/sdk'
import Swap from '../views/Swap'

const SwapPage = () => {
  return <Swap />
}

SwapPage.chains = CHAIN_IDS

SwapPage.chains = [ChainId.BSC, ChainId.BSC_TESTNET, ChainId.ETHEREUM, ChainId.ONUS_TESTNET]

export default SwapPage
