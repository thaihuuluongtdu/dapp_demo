import HomeDemo from 'views/HomeDemo'
import { ChainId } from '@pancakeswap/sdk'
import MarketplacePage from './marketplace'

const IndexPage = () => {
  return <MarketplacePage />
}
IndexPage.chains = [ChainId.BSC, ChainId.BSC_TESTNET, ChainId.ETHW_MAINNET]

export default IndexPage
