import { useTranslation } from '@pancakeswap/localization';
import { SerializedToken } from '@pancakeswap/sdk';
import { AddIcon, Button, Flex, IconButton, MinusIcon, Text, useModal, AutoRenewIcon } from "@pancakeswap/uikit";
import { Address } from "config/constants/types";
import useTheme from 'hooks/useTheme';
import { useApprove } from "state/poolrunV2/hook/useApprove"
import React from "react";
import Countdown, { zeroPad } from 'react-countdown'
import { GetApprovePoolContract } from "state/poolrunV2/fetchData";
import styled from "styled-components";
import StakeModal from "../../components/StakeModal";
import UnStakeModal from "../../components/UnStakeModal";


interface StakeActionProps {
    tokenStake:SerializedToken,
    poolContract:Address,
    chainId:number
    account:string|null
    refresh:number,
    startTimeStake: number,
    totalStakeAmount:string,
    onRefresh?: (newValue) => void 
}
const StakeAction: React.FC<StakeActionProps> = ({
    tokenStake,
    poolContract,
    chainId,
    account,
    refresh,
    startTimeStake,
    totalStakeAmount,
    onRefresh
}) => {
    const { t } = useTranslation()
    const { theme } = useTheme()
    const currentTime = Date.now()
    const isStartTimeStake = currentTime < startTimeStake*1000
    const [ openModalUnStake ] = useModal(
        <UnStakeModal
            title="UnStake"
            tokenStake={tokenStake}
            refresh={refresh}
            chainId={chainId}
            account={account}
            poolContract={poolContract}
            onRefresh={onRefresh}
        />
    )
    const [ openModalStake ] = useModal(
        <StakeModal
            title="Stake"
            tokenStake={tokenStake}
            refresh={refresh}
            chainId={chainId}
            account={account}
            poolContract={poolContract}
            onRefresh={onRefresh}
        />
    )
    const renderCountdown = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) return (
            <></>
        )
        return (
            <Flex ml="10px">
                <Text >{zeroPad(days)} days,</Text>
                <Text ml="5px" >{zeroPad(hours)}:</Text>
                <Text >{zeroPad(minutes)}:</Text>
                <Text >{zeroPad(seconds)}</Text>
            </Flex>
        )
    }
    const { handleApprove, pendingApprove } = useApprove(poolContract, tokenStake.address, chainId, onRefresh )
    const { allowance } = GetApprovePoolContract(tokenStake, poolContract, account, chainId, refresh )
    return (
        <Flex width="100%" flexDirection="column" mt="1rem">
            <Flex width="100%" flexDirection="column">
                { Number(allowance) === 0 ?
                    <ApprovedButton
                        disabled={pendingApprove}
                        onClick={handleApprove}
                        endIcon={pendingApprove ? <AutoRenewIcon spin color="textDisabled" /> : undefined}
                    >
                        {t("Approve")}
                    </ApprovedButton>
                :
                    <>
                        { Number(totalStakeAmount) === 0 ?
                            <StakeButton onClick={openModalStake} disabled={isStartTimeStake} >
                                Stake
                            </StakeButton>
                        :
                            <Container width="100%" justifyContent="space-between">
                                <Flex flexDirection="column">
                                    <Text><span style={{color:theme.colors.primaryBright}}>{tokenStake.symbol}</span> Staked</Text>
                                    <Text mt="6px" fontSize="18px" bold>{Number(totalStakeAmount).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
                                </Flex>
                                <Flex style={{gap:"15px"}}>
                                    <CustomIconButton
                                        onClick={openModalUnStake}
                                    >
                                        <MinusIcon color="white"/>
                                    </CustomIconButton>
                                    <CustomIconButton
                                        onClick={openModalStake}
                                    >
                                        <AddIcon color="white"/>
                                    </CustomIconButton>
                                </Flex>
                            </Container>
                        }
                    </>
                }
            </Flex>
            { isStartTimeStake &&
                <Flex mt="1rem">
                    <Text>Start in</Text>
                    <Countdown zeroPadTime={2} date={startTimeStake}  renderer={renderCountdown}/>
                </Flex>
            }
        </Flex>
    )
}
export default StakeAction

const StakeButton = styled(Button)`
    width: 100%;
    box-shadow:none;
    height:55px;
`
const CustomIconButton = styled(IconButton)`
    display: flex;
    justify-content:center;
    align-items:center;
`
const Container = styled(Flex)`
    border: 2px solid ${({ theme }) => theme.colors.cardBorder};
    border-radius: 12px;
    padding:10px;
`
const ApprovedButton = styled(Button)`
    width: 100%;
    height:55px;
`
