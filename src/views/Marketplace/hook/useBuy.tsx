import React, { useCallback, useState } from 'react'
import {useToast} from '@pancakeswap/uikit'
import { useTranslation } from '@pancakeswap/localization'
import { ToastDescriptionWithTx } from 'components/Toast'
import contract from 'config/constants/contracts'
import { getAddress } from 'utils/addressHelpers'
import { useDemoUseContract, useRunMarketplaceContract } from 'hooks/useContract'
import { useCallWithMarketGasPrice } from 'hooks/useCallWithMarketGasPrice'

export const useBuy = (saleId, chainId:number, contractAddress:string) => {
  const [requestedBuy, setRequestBuy] = useState(false)
  const { toastSuccess, toastError } = useToast()
  const { callWithMarketGasPrice } = useCallWithMarketGasPrice()
  const [ isClose, setClose ] = useState(false)
  const { t } = useTranslation()
  const marketplaceContract = useRunMarketplaceContract(getAddress(contract.runMarketplace, chainId));
  const [ pendingBuy, setPendingBuy ] = useState(false)
  const tokenAddress = useDemoUseContract(contractAddress)
  const handleBuy = useCallback(async () => {
    setPendingBuy(true)
    try {
      const tx = await callWithMarketGasPrice(marketplaceContract, 'buyItem', [saleId])
      const receipt = await tx.wait()

      if (receipt.status) {
        toastSuccess(
            t('Successfully purchase'),
          <ToastDescriptionWithTx txHash={receipt.transactionHash}/>
        )
        setClose(true)
        setRequestBuy(true)
      } else {
        // user rejected tx or didn't go thru
        toastError(t('Error'), t('Please try again. Confirm the transaction and make sure you are paying enough gas!'))
        setRequestBuy(false)
        
      }
    } catch (e) {
      console.error(e)
      toastError(t('Error'), t('Please try again. Confirm the transaction and make sure you are paying enough gas!'))
    } finally {
      setPendingBuy(false)
    }
  }, [callWithMarketGasPrice, marketplaceContract, saleId, toastSuccess, t, toastError])
 

  return { handleBuy, requestedBuy, pendingBuy, isClose }
}
