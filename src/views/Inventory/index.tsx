import { useTranslation } from '@pancakeswap/localization';
import { AutoRenewIcon, Button, Flex, Text } from '@pancakeswap/uikit'
import PageHeader from 'components/Layout/PageHeader'
import Page from 'components/Layout/Page'
import useTheme from 'hooks/useTheme'
import Container from 'components/Layout/Container'
import styled from 'styled-components'
import Row from 'components/Layout/Row';
import Select, { OptionProps } from 'components/Select/Select'
import { useState } from 'react';
import InventoryOnChain from './components/InventoryOnChain';
import InventoryOffChain from './components/InventoryOffChain';

 
const Inventory = () => { 
    const { t } = useTranslation()
    const { theme } = useTheme()
    const [ isOnChain, setIsOnChain] = useState(true)
    const [ tabIndex, setTabIndex ] = useState(0)
    const [ isBoxes, setIsBoxes ] = useState(true)
    const handleSelectChain = (option: OptionProps): void => {
        setIsOnChain(option.value)
    }
    const handleChainBoxes = (option: OptionProps): void => {
        setIsBoxes(option.value)
    }    
    return (
        <>
            <PageHeader 
                        nameTitle="RUN TOGETHER"
                        namePlace="Inventory"
                        imgIcon="/images/runInventory/imgBanner.png"
                        bgColor={theme.colors.yellow}
            />
            <Page>
                <CustomContainer>
                    <CsRow justify="space-between">
                        <CsFlex alignItems="center">
                            <Select
                                options={[
                                    {
                                        label: t('ON-CHAIN'),
                                        value: true,
                                    },
                                    {
                                        label: t('OFF-CHAIN'),
                                        value: false,
                                    },
                                ]}
                                onOptionChange={handleSelectChain}
                            />
                            { !isOnChain &&
                                <Select
                                    options={[
                                        {
                                            label: t('BOXES'),
                                            value: true,
                                        },
                                        {
                                            label: t('SHOES'),
                                            value: false,
                                        },
                                    ]}
                                    onOptionChange={handleChainBoxes}
                                />
                            }
                        </CsFlex>
                        <CustomRow>
                            <WrapperTabs>
                                <CotainerTabAll>
                                    <CustomTabAll isActive={ tabIndex === 0 ? !false : false} onClick={()=>setTabIndex(0)}>
                                        All
                                    </CustomTabAll>
                                </CotainerTabAll>
                                <CustomTab isActive={ tabIndex === 1 ? !false : false} onClick={()=>setTabIndex(1)}>
                                    <Tags src="/images/martketplace/11.png" alt="tag box"/>
                                    <Text ml="3px" color="#777E91" bold>MetaRush</Text>
                                    </CustomTab>
                                <CustomTab isActive={ tabIndex === 2 ? !false : false} onClick={()=>setTabIndex(2)}>
                                    <Tags src="/images/martketplace/22.png" alt="tag box"/>
                                    <Text ml="3px" color="#777E91" bold>MetaRun</Text>
                                </CustomTab>
                                <CustomTab isActive={ tabIndex === 3 ? !false : false} onClick={()=>setTabIndex(3)}>
                                    <Tags src="/images/martketplace/33.png" alt="tag box"/>
                                    <Text ml="3px" color="#777E91" bold>MetaRace</Text>
                                </CustomTab>
                                <CustomTab isActive={ tabIndex === 4 ? !false : false} onClick={()=>setTabIndex(4)}>
                                    <Tags src="/images/martketplace/44.png" alt="tag box"/>
                                    <Text ml="3px" color="#777E91" bold>MetaRich</Text>
                                </CustomTab>
                            </WrapperTabs>                        
                        </CustomRow>
                    </CsRow>
                    { isOnChain ?
                        <InventoryOnChain filter={tabIndex}/>
                        :
                        <InventoryOffChain isListBoxes={isBoxes} filterType={tabIndex} />
                    }
                </CustomContainer>
            </Page>
        </>        
    );
}

export default Inventory

const CustomContainer = styled(Container)`
    width:100%;
    height: auto;
    @media only screen and (max-width: 768px) {
        padding-left:10px;
        padding-right: 10px;
    }
`
const CsFlex = styled(Flex)`
    flex-wrap:wrap;
    gap: 30px;
    @media only screen and (min-width: 600px) and (max-width: 1024px){
        width: 100%;
        justify-content: space-between;
        margin-bottom: 1rem;
    }
    @media only screen and (max-width: 600px) {
        width: 100%;
        gap: 20px;
    }
` 
const CustomRow = styled(Flex)`
    flex-wrap:wrap;
    @media only screen and (max-width:1080px) {
        width: 100%;
        justify-content: center;
    }
`
const WrapperTabs = styled(Flex)`
    width:auto;
    @media only screen and (min-width: 600px) and (max-width: 1080px) {
        width: 100%;
        justify-content: space-between;
    }
    @media only screen and (max-width: 600px) {
        width: 100%;
        flex-wrap:wrap;
    }
`
const CotainerTabAll = styled.div`
    display: flex;
    width:auto;
    @media only screen and (max-width: 600px) {
        width: 100%;
        justify-content: center;
    }
`
const CustomTabAll = styled(Button)<{isActive?:boolean}>`
    width: auto;
    background:none;
    box-shadow: none;
    font-size:22px;
    display: flex;
    justify-content: flex-start;
    padding-left:10px;
    font-weight:bold;
    border-radius:0px;
    color:${({ isActive }) => isActive ? "#4B19F5" : "#B1B5C3"};
    border-bottom:${({ isActive }) => isActive ? "3px solid #4B19F5" : "none"}; 
    @media only screen and (max-width: 600px) {
        width: 50%;
        justify-content: center;
        margin-bottom:10px;
    }
`
const CustomTab = styled(Button)<{isActive?:boolean}>`
    width: auto;
    background:none;
    box-shadow: none;
    font-size:22px;
    display: flex;
    justify-content: flex-start;
    padding-left:10px;
    font-weight:bold;
    border-radius:0px;
    color:${({ isActive }) => isActive ? "#4B19F5" : "#B1B5C3"};
    border-bottom:${({ isActive }) => isActive ? "3px solid #4B19F5" : "none"}; 
    @media only screen and (max-width: 600px) {
        width: 50%;
        padding: 0px;
        &:first-child {
            width: 100%;
        }
        margin-bottom:10px;
    }
`
const Tags = styled.img`
    height: 35px;
    width: 35px;
    border-radius:50%;
    overflow:hidden;
`
const CsRow = styled(Row)`
    @media screen and (max-width: 1024px){
        flex-direction: column;
    }
    @media screen and (max-width: 768px){
        flex-direction: column;
    }
    @media screen and (max-width: 600px){
        flex-direction: column;
    }
`