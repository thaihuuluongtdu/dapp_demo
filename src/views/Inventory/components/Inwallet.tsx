import React, { useEffect, useState } from "react";
import { Text, Flex, Button } from "@pancakeswap/uikit";
import { GetBalanceNft, GetNftInfo, GetTokenIds } from "state/marketplace";
import ReactPaginate from 'react-paginate';
import styled from "styled-components";
import { useTranslation } from "@pancakeswap/localization";
import useActiveWeb3React from "hooks/useActiveWeb3React";
import { ChainId } from '@pancakeswap/sdk';
import { bscTokens, ethwTokens, bscTestnetTokens } from '@pancakeswap/tokens';
import Card from "./Card";


interface Props{
    filterBoxType?:number
}
const Inwallet:React.FC<Props> = ({filterBoxType}) => {
    function renderTokenByChain(chainId){
        if( chainId === ChainId.BSC ) {
            return bscTokens.runtogetherBoxNft.address
        } if (chainId === ChainId.ETHW_MAINNET) {
            // return ethwTokens.runtogetherBoxNft.address
            return ''
        } if (chainId === ChainId.BSC_TESTNET) {
            return bscTestnetTokens.runtogetherBoxNft.address
        }
        return ""
    }
    const { account } = useActiveWeb3React()
    const { chainId } = useActiveWeb3React()
    const itemsPerPage = 9
    const { t } = useTranslation()
    const tokenAddress = renderTokenByChain(chainId)
    const [pageCount, setPageCount] = useState(0)
    const [itemOffset, setItemOffset] = useState(1)
    const [currentItems, setCurrentItems] = useState([])   
    const [ balance ] = GetBalanceNft(tokenAddress, account, chainId)
    const [ tokenIds ] = GetTokenIds(tokenAddress, account, balance, chainId) 
    const [ nftInfo ] = GetNftInfo(tokenAddress, tokenIds, chainId)
    const [ listNft, setListNft ] = useState([...nftInfo])
    useEffect(() => {
        if(filterBoxType === 0){
            setListNft([...nftInfo])
        } if(filterBoxType !== 0)  {
            setListNft(nftInfo.filter((data) => data.nftType === filterBoxType))
        }
    }, [nftInfo, filterBoxType])
    // panigate
    function MovetoTop(){
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    }
    const handlePageClick = (event) => {
        const newOffset = (event.selected * itemsPerPage) % listNft.length;
          setItemOffset(newOffset);
    };
    useEffect(() => {
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(listNft.slice(itemOffset, endOffset));
          setPageCount(Math.ceil(listNft.length / itemsPerPage));
    }, [itemOffset, itemsPerPage, listNft]);
    useEffect(() => {
            setItemOffset(0);
    }, [listNft]);
    return (
        <Flex width="100%" flexDirection="column" mt="1rem" height="auto" minHeight="70vh">
            <Flex width="100%" justifyContent="space-around" flexWrap="wrap">
                {currentItems.length !== 0 ?
                    <>
                        {currentItems.map((item) => {
                            return (
                                <Card 
                                    ID={item.nftId}
                                    boxType={item.nftType}
                                    IsHaving={!false}
                                    onChain={!false}
                                />
                            )
                        })}
                    </>
                :
                        <Text mt="2rem">{t("No Data")}</Text>
                }
                
            </Flex>
            <CustomFlex width="100%" mt="1rem" justifyContent="center" height="62px">
                <ReactPaginate
                    nextLabel=">"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={3}
                    marginPagesDisplayed={2}
                    pageCount={pageCount}
                    previousLabel="<"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                    renderOnZeroPageCount={null}
                    onClick={MovetoTop}
                />
            </CustomFlex>
        </Flex>
    )
}
export default Inwallet

const CustomFlex = styled(Flex)`
    margin-bottom:1.5rem;
    .pagination{
        display:flex;
        flex-direction: row;
        width:500px;
        justify-content:space-around;
        align-items:center;
        @media screen and (max-width: 600px){
            width: 100%;
        }
        *{
            list-style-type: none;
        }
    }
    .page-link {
        background:${({ theme }) => theme.colors.tertiary};
        padding:12px;
        border-radius:5px !important;
        border:none !important;
        color:${({ theme }) => theme.colors.text};
        &:focus {
            box-shadow:none !important;
        }
        &:hover{
            background:${({ theme }) => theme.colors.backgroundTab};
        }
    }
    .page-item.disabled .page-link{
        background:${({ theme }) => theme.colors.disabled};
        cursor: not-allowed! important;
        opacity: 0.7;
        pointer-events:none;
    }
    .page-item.active .page-link{
        background:${({ theme }) => theme.colors.primaryBright};
        color:#fff;
    }
`