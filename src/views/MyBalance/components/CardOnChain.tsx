import React from "react";
import styled from "styled-components";
import { Text, Flex, WalletIcon } from "@pancakeswap/uikit";
import { GetTokenBalance } from "utils/getTokenBalance";
import { bscTokens, ethwTokens, bscTestnetTokens } from '@pancakeswap/tokens'
import { ChainId } from '@pancakeswap/sdk'
import { useWeb3React } from '@pancakeswap/wagmi'
import useActiveWeb3React from 'hooks/useActiveWeb3React'
import { useTranslation } from '@pancakeswap/localization'

const CardOnChain = () => {
    function renderTokenByChain(chainId){
        if( chainId === ChainId.BSC ) {
            return bscTokens.runtogether.address
        } if (chainId === ChainId.ETHW_MAINNET) {
            return ethwTokens.runtogether.address
        } if (chainId === ChainId.BSC_TESTNET) {
            return bscTestnetTokens.runtogether.address
        }
        return ""
    }
    const { chainId } = useActiveWeb3React()
    const { t } = useTranslation()
    const tokenAddress = renderTokenByChain(chainId)
    const { account } = useWeb3React()
    const { balance } = GetTokenBalance(tokenAddress, account, chainId)
    return(
        <Container>
            <Flex width="100%" alignItems="center">
                <WalletIcon filterColor="#F86F61"/>
                <Text bold fontSize="18px" ml="10px" color="#fff"> On-chain</Text>
            </Flex>
            <Flex flexDirection="column" width="100%">
                <Text fontSize="18px" bold color="#fff">On-wallet</Text>
                <Flex alignItems="center">
                    { account ?
                        <Text bold color="#fff" mr="10px">{Number(balance).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
                    :
                        <Text color="#fff" mr="10px" >{t("No Data")}</Text>    
                    }
                    <img src="/images/tokens/0xc643E83587818202E0fFf5eD96D10Abbc8Bb48e7.png" alt="logo runtogether" style={{width:"30px", height:"30px"}}/>
                </Flex>
            </Flex>
        </Container>
    )
}

export default CardOnChain

const Container = styled(Flex)`
    justify-content: space-around;
    align-items: center;
    flex-direction:column;
    height: 160px;
    width:100%;
    border-radius:12px;
    background: ${({ theme }) => theme.colors.warning43}; 
    padding:18px 22px 18px 22px;
`