import React, { useState, useEffect } from "react";
import { Text, Flex, Button, Modal, AutoRenewIcon, BalanceInput, Box, WarningIcon } from "@pancakeswap/uikit";
import styled from "styled-components";
import BigNumber from "bignumber.js";
import { useTranslation } from "@pancakeswap/localization";
import { usePriceRunBusd } from 'state/farms/hooks'
import { formatNumber } from 'utils/formatBalance'
import { useUnStaked } from "../hook/useUnStake"
import { GetTotalStakedByUser } from "../hook/fetchDataPool"

interface UnStakeModalProps {
    onDismiss?: () => void
    title?:string
    contractAddress:string
    onRefresh?:(newValue) => void,
    account:string,
    chainId:number
  }

const UnStakeModalPoolStore: React.FC<UnStakeModalProps> = ({onDismiss, title, contractAddress, onRefresh, account, chainId}) => {
    const { t } = useTranslation()
    const [unstakeAmount, setUnsunstakeAmount] = useState('')
    const handleStakeInputChange = (input: string) => {
        setUnsunstakeAmount(input)
    }
    const [ refresh, setRefresh ] = useState(0)
    function onUpdateAmount(newValue:number){
        setRefresh(newValue)
    }
    const { handleUnStaked, requestedUnStake, pendingUnStaked } = useUnStaked(unstakeAmount, contractAddress, onUpdateAmount, onRefresh, account, chainId)
    const { totalStakeByUser } = GetTotalStakedByUser(contractAddress, refresh, account, chainId)
    const runPriceUsd = usePriceRunBusd().toNumber()
    const [percent, setPercent ] = useState(0)
    function quickSelect (value:number){
        setPercent(value)
        if ( value === 100 ) {
            setUnsunstakeAmount(totalStakeByUser)
        } else {
            setUnsunstakeAmount((Math.floor(Number(totalStakeByUser) * value) / 100).toString())
        }
    }
    const formattedUsdValueStaked = unstakeAmount ? formatNumber(Number(unstakeAmount)*runPriceUsd) : ''
    useEffect(() => {
        function updateAmount(){
            setUnsunstakeAmount("")
        }
        if ( requestedUnStake ) {
            updateAmount()
        }
    }, [requestedUnStake, refresh])
    useEffect(() => {
        const percentUnStake = (Number((Math.floor(Number(unstakeAmount) * 100) / 100))/Number((Math.floor(Number(totalStakeByUser) * 100) / 100)))*100
        setPercent(Number(percentUnStake.toFixed(0)))
    }, [unstakeAmount, totalStakeByUser]) 
    const convertunstakeAmount = new BigNumber(unstakeAmount)
    const convertTotalStakeByUser = new BigNumber(totalStakeByUser)
    const isFullAmount = convertunstakeAmount.isGreaterThan(convertTotalStakeByUser)
    return (
        <CustomModal title="" onDismiss={onDismiss}>
            <Container>
                <Text textAlign="center" fontSize="22px" bold>{title}</Text>
                <Flex width="100%" flexDirection="column" mt="1rem">
                    <Text>{title}</Text>
                    <CsBalanceInput
                        value={unstakeAmount}
                        onUserInput={handleStakeInputChange}
                        decimals={18}
                        currencyValue={runPriceUsd && `~${formattedUsdValueStaked || 0} BUSD`}
                    />
                </Flex>
                <Flex width="100%" mt="10px" mb="10px">
                    <Flex style={{gap:"4px"}}>
                        <Text>Total staked:</Text>
                        <Text>{((Math.floor(Number(totalStakeByUser) * 100) / 100)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</Text>
                    </Flex>
                </Flex>
                <Flex width="100%" justifyContent='space-between' flexWrap="wrap" style={{gap:"15px"}}>
                    <CustonButton isActive={percent === 25} onClick={()=>quickSelect(25)}>25%</CustonButton>
                    <CustonButton isActive={percent === 50} onClick={()=>quickSelect(50)}>50%</CustonButton>
                    <CustonButton isActive={percent === 75} onClick={()=>quickSelect(75)}>75%</CustonButton>
                    <CustonButton isActive={percent === 100} onClick={()=>quickSelect(100)}>100%</CustonButton>
                </Flex>
                { isFullAmount &&
                    <Flex style={{gap:"5px"}} alignItems="center" mt="10px">
                        <WarningIcon color="warning43"/>
                        <Text small color="warning43">{t("Withdrawal amount is greater than deposit amount")}</Text>
                    </Flex>
                }
                <ConfirmButton 
                    disabled={pendingUnStaked || Number(unstakeAmount) === 0 || isFullAmount}
                    onClick={handleUnStaked}
                    endIcon={pendingUnStaked ? <AutoRenewIcon spin color="textDisable" /> : null}
                >
                    Confirm
                </ConfirmButton>
            </Container>
        </CustomModal>
    )
}
export default UnStakeModalPoolStore
const CustomModal = styled(Modal)`
    padding:0px;
`
const Container = styled(Flex)`
    width: 400px;
    flex-direction:column;
    @media screen and (max-width: 600px) {
        width: 300px !important;
    }
`
const CustonButton = styled(Button)<{isActive?:boolean}>`
    width: auto;
    height: 40px;
    box-shadow:none;
    background: ${({ isActive, theme }) => isActive ? theme.colors.primaryBright : "transparent"};
    border: 2px solid ${({ isActive, theme }) => isActive ? "transparent" : theme.colors.cardBorder};
    border-radius: 90px;
    color:${({ isActive, theme }) => isActive ? theme.colors.white : theme.colors.text};
    @media screen and (max-width: 600px) {
        width: 45% !important;
    }
`
const ConfirmButton = styled(Button)`
    width:100%;
    border-radius:90px;
    background: ${({ theme }) => theme.colors.primaryBright};
    color:${({ theme }) => theme.colors.white};
    margin-top:1.25rem;
    box-shadow:none;
`
const CsBalanceInput = styled(BalanceInput)`
    ${Box} {
        width: 100% !important;
    }
`